﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace WithExpressions
{
    class Program
    {
        public static void Main(string[] args)
        {
            const int param1 = 7;
            const int param2 = 11;

            System.Console.WriteLine("{0} > {1} = {2}", param1, param2, GreaterThan(param1, param2));
            Console.ReadLine();

            System.Console.WriteLine("{0} > {1} = {2}", param1, param2, GreaterThanDelegate(param1, param2));
            Console.ReadLine();

            System.Console.WriteLine("{0} > {1} = {2}", param1, param2, GreaterThanAsExpression.Compile()(param1, param2));
            Console.ReadLine();

            System.Console.WriteLine("{0} > {1} = {2}", param1, param2, GreaterThanFullExpression()(param1, param2));
            Console.ReadLine();

            Console.WriteLine("{0} {1} {2} = {3}", param1, "<", param2, DoTheMath(param1, "<", param2));
            Console.ReadLine();
        }

        static Expression<Func<int, int, bool>> GreaterThanAsExpression = (left, right) => left > right;

        static Func<int, int, bool> GreaterThanDelegate = (left, right) => left > right;

        public static bool GreaterThan(int left, int right)
        {
            return left > right;
        }

        static Func<int, int, bool> GreaterThanFullExpression()
        {
            var left = Expression.Parameter(typeof(int), "left");
            var right = Expression.Parameter(typeof(int), "right");
            var greaterThanExpr = Expression.Lambda<Func<int, int, bool>>(Expression.GreaterThan(left, right), left, right);
            return greaterThanExpr.Compile();
        }


        public static bool DoTheMath<T>(T a, string op, T b)
        {
            var left = Expression.Parameter(typeof(T), "left");
            var right = Expression.Parameter(typeof(T), "right");
            var expression = Expression.Lambda<Func<T, T, bool>>
              (
                  op.Equals(">") ? Expression.GreaterThan(left, right) :
                  op.Equals("<") ? Expression.LessThan(left, right) :
                  op.Equals(">=") ? Expression.GreaterThanOrEqual(left, right) :
                  op.Equals("<=") ? Expression.LessThanOrEqual(left, right) :
                  op.Equals("!=") ? Expression.NotEqual(left, right) :
                  Expression.Equal(left, right), left, right
              );
            return expression.Compile()(a,b);
        }
    }
}