﻿using System;
using System.CodeDom;
using System.CodeDom.Compiler;
using System.IO;
using System.Text;

namespace Metaprogramming.WithCodeDom
{
    class Program
    {
        public static void Main(string[] args)
        {
            Console.WriteLine(GenerateCode(HelloWorldProgram(), "vb"));
            Console.ReadLine();
            Console.WriteLine(GenerateCode(HelloWorldProgram(), "C#"));
            Console.ReadLine();                 
        }        

        private static string GenerateCode(CodeNamespace prgNamespace, string lang)
        {
            var compilerOptions = new CodeGeneratorOptions()
            {
                IndentString = " ",
                BracingStyle = "C",
                BlankLinesBetweenMembers = false
            };
            var codeText = new StringBuilder();
            using (var codeWriter = new StringWriter(codeText))
            {
                CodeDomProvider.CreateProvider(lang)
                  .GenerateCodeFromNamespace(
                    prgNamespace, codeWriter, compilerOptions);
            }
            return codeText.ToString();
        }

        public static CodeNamespace HelloWorldProgram()
        {
            var ns = new CodeNamespace("MetaWorld");
            var systemImport = new CodeNamespaceImport("System");
            var programClass = new CodeTypeDeclaration("Program");            
            var methodMain = new CodeMemberMethod { Attributes = MemberAttributes.Static, Name = "Main" };
            var methodMainStatements = new CodeMethodInvokeExpression(new CodeSnippetExpression("Console"), "WriteLine", new CodePrimitiveExpression("Hello, world!"));
            
            ns.Imports.Add(systemImport);
            ns.Types.Add(programClass);
            programClass.Members.Add(methodMain);
            methodMain.Statements.Add(methodMainStatements);

            return ns;
        }
    }
}