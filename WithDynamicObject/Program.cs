﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;

namespace WithDynamicObject
{
    class Program
    {
        static void Main(string[] args)
        {
            dynamic container = new ExpandoObject();
            container.Name = "Thiago";
            container.PhoneNumber = 8675309;

            Console.WriteLine(container.Name);
            Console.WriteLine(container.PhoneNumber);
            Console.ReadLine();
             
            ((IDictionary<string, object>)container)["Name"] = "Thiago F.";

            Console.WriteLine(container.Name);
            Console.WriteLine(container.PhoneNumber);
            Console.ReadLine();
        }
    }

    public class MyExpandoObject : DynamicObject
    {
        private Dictionary<string, object> _dict = new Dictionary<string, object>();

        public override bool TryGetMember(GetMemberBinder binder, out object result)
        {
            result = null;
            if (_dict.ContainsKey(binder.Name.ToUpper()))
            {
                result = _dict[binder.Name.ToUpper()];
                return true;
            }
            return false;
        }

        public override bool TrySetMember(SetMemberBinder binder, object value)
        {
            if (_dict.ContainsKey(binder.Name.ToUpper()))
                _dict[binder.Name.ToUpper()] = value;
            else
                _dict.Add(binder.Name.ToUpper(), value);
            return true;
        }
    }
}